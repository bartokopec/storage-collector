/* eslint-disable no-console */

import { MobileProject } from '@trapezedev/project';
import appInfo from '../package.mjs';
import process from 'node:process';

/** @type {import('@trapezedev/project').MobileProjectConfig} */
const config = {
  android: {
    path: 'android'
  }
};

console.log('\n\nUPDATING Androidmanifest.xml ...');

const project = new MobileProject(process.cwd(), config);
await project.load();
await project.android?.setVersionName(appInfo.version);
const code = appInfo.version.split('.').join('');
await project.android?.setVersionCode(code);
await project.commit();

console.log('WROTE VERSION TO MANIFEST\n\n');
