/* eslint-disable no-console */

import { exec } from "child_process";
import dotenv from 'dotenv'
import process from 'node:process';

dotenv.config()

const javaVersion = process.env.JAVA_VERSION

console.log('\n\nCHECKING JAVA VERSION...');

exec('java -version',(err, stdout, stderr)=>{
  const output = stdout + stderr
  const versionRegex = /"([0-9.]+)"/
  const searchResult = output.match(versionRegex)

  if(!searchResult || !searchResult[1]){
    console.error('Could not read Java version')
    process.exit(1)
  }

  const foundVersion = searchResult[1]

  if(!foundVersion.startsWith(javaVersion)){
    console.error('Wrong Java version!')
    console.error('Current Java version:', foundVersion)
    console.error('but App requires:', javaVersion)
    process.exit(1)
  }

  console.log('Correct Java version found! Lets go!\n\n');
})