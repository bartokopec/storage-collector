/* eslint-disable no-console */

import dotenv from 'dotenv'
import { exec } from "child_process";
import appInfo from '../package.mjs';
import fs from 'fs';
import process from 'node:process';

try {
  if (!fs.existsSync('release')) {
    fs.mkdirSync('release');
  }
} catch (error) {
  console.error(error)
}

dotenv.config()

const path = 'android/app/build/outputs/apk/release/app-release-unsigned.apk'
const opt = {
  keystorePath: process.env.VITE_KEYSTORE_PATH,
  keystorePassword: process.env.VITE_KEYSTORE_PASS,
  keystoreAlias: process.env.VITE_KEYSTORE_ALIAS,
  keystoreAliasPassword: process.env.VITE_KEYSTORE_ALIAS_PASS,
  releaseType: 'APK',
  signingType: 'apksigner'
}
const code = appInfo.version.split('.').join('');

console.log('\n\nRunning apksigner ...');

exec(`java -jar scripts/apksigner.jar sign -v --ks "${opt.keystorePath}" --ks-key-alias ${opt.keystoreAlias} --ks-pass pass:${opt.keystorePassword} --key-pass pass:${opt.keystoreAliasPassword} --in ${path} --out release/${appInfo.appId}-${code}.apk`, (error, stdout, stderr) => {
  if (error) {
    console.error(`error: ${error.message}`);
    return;
  }
  if (stderr) {
    console.error(`stderr: ${stderr}`);
    return;
  }
  console.log(`Result: ${stdout}`);
});
