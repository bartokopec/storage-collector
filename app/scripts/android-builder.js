/* eslint-disable no-console */

import dotenv from 'dotenv'
import { spawnSync } from "child_process";
import fs from 'fs';
import process from 'node:process';

dotenv.config()

if(!process.env.ANDROID_HOME || !fs.existsSync(process.env.ANDROID_HOME)){
  console.error('Set valid ANDROID_HOME variable!')
  process.exit(1)
}

console.log('\n\nCLEANING ANDROID ...');

spawnSync('./android/gradlew', ['clean','-p=android/'],{
  stdio: 'inherit'
});

console.log('\n\nBUILDING ANDROID ...');

spawnSync('./android/gradlew', ['assemble','-p=android/'],{
  stdio: 'inherit'
});