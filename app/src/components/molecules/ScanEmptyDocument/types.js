/**
 * @typedef ScanEmptyDocumentProps
 * @property {(docNumber: string)=>void} submit
 */