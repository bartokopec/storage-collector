import { focusInp } from '@/libs/Focus';
import { useTranslator } from '@/libs/Translator';
import { ref } from 'vue';
import './types'

/** @param {ScanEmptyDocumentProps} props  */
export function useController(props) {  
  const { dictionary } = useTranslator();
  const docNumber = ref('')
  const docNumberInputId = 'document-number'

  setTimeout(() => {
    focusInp(docNumberInputId);
  }, 500);

  function onConfirm() {
    props.submit(docNumber.value);
  }

  return {
    dictionary,
    docNumber,
    docNumberInputId,
    onConfirm
  }
}