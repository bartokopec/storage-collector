/**
 * @typedef EditItemModalProps
 * @property {string} place
 * @property {number} count
 * @property {(data: {count: number, place: string})=>void} onConfirm
 */