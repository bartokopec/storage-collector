import { ref } from 'vue';
import { useTranslator } from '@/libs/Translator';
import { focusInp } from '@/libs/Focus';
import './types'

/** @param {EditItemModalProps} props  */
export function useController(props) {
  const storageCode = ref(props.place || '');
  const count = ref(props.count || 0);
  const { dictionary } = useTranslator();
  
  setTimeout(() => {
    focusInp('edit-count-modal-id');
  }, 500);
  
  function confirm() {
    const data = {
      count: count.value,
      place: storageCode.value
    };
    props.onConfirm(data);
  }
  
  function incrementCount() {
    count.value += 1;
  }
  
  function decreaseCount() {
    if (count.value > 1) {
      count.value -= 1;
    }
  }
  
  return {
    dictionary,
    storageCode,
    count,
    confirm,
    incrementCount,
    decreaseCount
  }
}