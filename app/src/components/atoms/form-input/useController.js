/**
 * @param {import('./type').FormInputPropsType} props 
 * @param {import("./type").FormInputEventsType} emit 
 */
export function useController(props, emit) {  
  /** @param {string} val */
  function onInputChange(val) {
    emit('update', props.capitalize 
      ? val.toUpperCase() 
      : val);
  }

  function submit() {
    if(props.regex && !!props.value.length && !props.value.match(props.regex)){
      emit('reportUnmatchedValue', props.value);
      onInputChange('')
      return;
    }

    emit('submit');
  }

  return {
    onInputChange,
    submit
  }
}