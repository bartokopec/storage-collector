export const FormInputProps = {
  capitalize: Boolean,
  autofocus: Boolean,
  error: String,
  regex: String,
  id: String,
  min: Number,
  type: String,
  class: String,
  value: undefined
}

/**
 @typedef FormInputPropsType
 @property {string|number} value
 @property {string} error
 @property {string} regex
 @property {string} id
 @property {string} type
 @property {string} class
 @property {boolean} capitalize
 @property {boolean} autofocus
 @property {number} min
 */

export const FormInputEvents = [
  'update',
  'reportUnmatchedValue',
  'submit'
]

/**
  @typedef {
  (key:'update', value: string)=>void
  |(key:'reportUnmatchedValue', value: string)=>void
  |(key:'submit')=>void
  } FormInputEventsType
 */