import Modal from './Modal/Modal.vue'
import FormInput from './form-input/FormInput.vue'

export {
  Modal,
  FormInput
}