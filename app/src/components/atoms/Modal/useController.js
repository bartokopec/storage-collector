import {
  modalController
} from '@ionic/vue';
import { useTranslator } from '@/libs/Translator';

/** 
 * @param {()=>void} onConfirm  
*/
export function useController(onConfirm) {
  const { dictionary } = useTranslator();

  function cancel() {
    modalController.dismiss(null, 'cancel');
  }

  function confirm() {
    modalController.dismiss(null, 'confirm');
    onConfirm()
  }  
  
  return {
    dictionary,
    cancel,
    confirm
  }
}