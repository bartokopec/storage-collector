export default class StorageItem {
  /**
   * @typedef {import('./StorageWorker').default} StorageWorker
   * @param {StorageWorker} worker
   * @param {string} placeCode
   * @param {string} productCode
   * @param {number} planToScan If value is -1, then it's new Item in new Document and we dont know how many to scan
   * @param {number} scanned
   */
  constructor(worker, placeCode, productCode, planToScan, description, scanned = 0) {
    this.worker = worker;
    this.placeCode = placeCode;
    this.productCode = productCode;
    this.planToScan = planToScan;
    this.scanned = scanned;
    this.description = description;
  }

  static toDTO(item) {
    return {
      userCode: item.worker.userCode,
      docNumber: item.worker.docNumber,
      placeCode: item.placeCode,
      productCode: item.productCode,
      count: item.scanned
    };
  }

  /**
   * @typedef {import('./StorageWorker').default} StorageWorker
   * @param {StorageWorker} worker
   * @param {{barcode: string, quantity: number, description: string}} docItem
   * @returns {StorageItem}
   */
  static fromDocumentInfo(worker, docItem) {
    return new StorageItem(worker, null, docItem.barcode, docItem.quantity, docItem.description);
  }
}
