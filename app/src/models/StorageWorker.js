export default class StorageWorker {
  constructor(userCode, docNumber, isNewDocument = false) {
    this.userCode = userCode;
    this.docNumber = docNumber;
    this.isNewDocument = isNewDocument
  }
}
