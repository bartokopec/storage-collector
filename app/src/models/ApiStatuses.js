export const Statuses = {
  credsNotFound: 'credsNotFound',
  success: 'success',
  unreachable: 'unreachable',
  badResponse: 'badResponse',
  badRequest: 'badRequest',
  notFound: 'notFound',
  unauthorization: 'unauthorization',
  unknown: 'unknown'
};
