export class ApiRequest {
  /**
   * @param {'GET' | 'POST'} method
   * @param {string} endpoint
   * @param {any | Array} data
   */
  constructor(method, endpoint, data = undefined) {
    this.method = method;
    this.endpoint = endpoint;
    this.data = data;
  }
}
