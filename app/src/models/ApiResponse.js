export class ApiResponse {
  constructor(status, payload) {
    this._status = status;
    this._payload = payload;
  }

  getStatus() {
    return this._status;
  }
  getPayload() {
    return this._payload;
  }
}
