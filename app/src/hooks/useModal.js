import {
  modalController,
} from '@ionic/vue';

/** 
 * #//@param {import('@ionic/core').ComponentRef} modalComponent 
 */
export function useModal(modalComponent) {
  /** @param {import('@ionic/core').ComponentProps} props */
  async function openModal(props){
    const modal = await modalController.create({
      component: modalComponent,
      componentProps: props
    });
    modal.present();
    await modal.onWillDismiss();
  }

  return {
    openModal
  }
}