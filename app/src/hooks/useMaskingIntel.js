import '@/libs/types'

/**
 * Use masking intelligence
 * @param {MaskingIntelligenceData} data
 */
export function useMaskingIntel(data) {
  /**
   *  @param {string} value   
  */
  function findMatchMask(value){
    for (const {id, mask} of data) {
      if(value.match(mask)){
        /** @type {import('@ionic/vue').IonInput} */
        const container = document.getElementById(id);                
        if (container) {
          container.onInput({target:{value}});
        }
        break;
      }
    }
  }
  return {
    findMatchMask
  }
}