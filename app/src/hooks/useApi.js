import { Statuses } from '../models/ApiStatuses';
import { http } from '../libs/Api';
import { showToast } from '../libs/Toast';
import { routes } from '../router';
import { useTranslator } from '@/libs/Translator';

/**
 * @typedef {import('./../models/ApiRequest').default} ApiRequest
 * @typedef {import('./../models/ApiResponse').default} ApiResponse
 * @param {ApiRequest} request
 * @param {(response: ApiResponse)=>void} onSuccess
 * @param {(response: ApiResponse)=>void} onBadRequest
 */
export async function useApi(request, onSuccess, onBadRequest = () => {}) {
  const router = (await import('../router')).default;
  const { dictionary } = useTranslator();

  const response = await http(request);
  const status = response.getStatus();

  if (status === Statuses.success) {
    onSuccess(response);
  } else if (status === Statuses.badRequest) {
    onBadRequest(response);
  } else {
    router.push(routes.settings.path);
    await showToast(dictionary.value[status]);
  }
}
