import { useSlots } from 'vue';
import { useTranslator } from '@/libs/Translator';
// import { useCameraScan } from '@/libs/Scanner';
// import { useStorage } from '@/libs/LocalStorage';

export const useLayout = () => {

  const slots = useSlots();
  const hasFooter = !!slots['footer']
  // const storage = useStorage();
  const { dictionary } = useTranslator();
  // const { setCanCameraScan } = useCameraScan();

  // storage.service.get(storage.keys.cameraScan).then((value) => setCanCameraScan(value));

  return {
    hasFooter,
    dictionary
  }
}