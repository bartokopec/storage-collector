import {EditItemModal} from '@/components/molecules';
import { useStorage } from '@/libs/LocalStorage';
import { useModal } from '@/hooks/useModal';

/**
 * @param {import("vue").Ref<import('@/models/StorageItem').default[], import('@/models/StorageItem').default[]>} items 
 * @returns {{openEditModal: Promise<void>}}
 */
export function useEditModal(items){
  const storage = useStorage();
  const {openModal} = useModal(EditItemModal)

  /**
   * @param {import('@/models/StorageItem').default} item 
   * @param {number} index 
   */
  async function openEditModal(item, index) {
    await openModal({
      place: item.placeCode,
      count: item.scanned,
      onConfirm: async (data) => {
        items.value[index] = {
          ...item,
          scanned: data.count,
          placeCode: data.place
        };
        await storage.service.set(storage.keys.items, JSON.stringify(items.value));
      }
    })
  }

  return {
    openEditModal
  }
}