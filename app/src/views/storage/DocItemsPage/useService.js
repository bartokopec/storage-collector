import { useApi } from '@/hooks/useApi';
import { ApiRequest } from '@/models/ApiRequest';
import { env } from '@/libs/envs';
import { useStorage } from '@/libs/LocalStorage';
import StorageItem from '@/models/StorageItem';
import '@/libs/types'

export function useService(){
  const storage = useStorage();

  /**
   * @param {import('@/models/StorageWorker').default} worker 
   * @returns {Promise<StorageItem[]>}
   */
  function fetchItems(worker){
    return new Promise((resolve)=>{
      return useApi(
        new ApiRequest('GET', env.API_ROUTE_DOCINFO, {
          id: worker.userCode,
          doc: worker.docNumber
        }),
        async (response) => {
          /** @type {DocInfoResponse} */
          const data = response.getPayload();
          const items = data.document.map(item=>StorageItem.fromDocumentInfo(worker, item))
          resolve(items)
          await storage.service.set(storage.keys.items, JSON.stringify(items));   
        }
      );
    })
  }

  /**
   * @param {StorageItem[]} items 
   */
  function sendItems(items){
    return new Promise((resolve)=>{
      return useApi(
        new ApiRequest('POST', env.API_ROUTE_STORAGE, {
          items: items.map((item) => StorageItem.toDTO(item))
        }),
        () => {
          resolve()
        }
      );
    })
  }
  
  return {
    fetchItems,
    sendItems
  }
}