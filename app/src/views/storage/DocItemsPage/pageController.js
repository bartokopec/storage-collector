import { useTranslator } from '@/libs/Translator';
import { useStorage } from '@/libs/LocalStorage';
import {
  onIonViewWillEnter
} from '@ionic/vue';
import { ref } from 'vue';
import { useRouter } from 'vue-router';
import { showToast } from '@/libs/Toast';
import { routes } from '@/router';
import StorageWorker from '@/models/StorageWorker';
import { useService } from './useService';
import { useEditModal } from './useEditModal';

export function pageController(){
  /** @type {import('vue').Ref<import('@/models/StorageItem').default[], import('@/models/StorageItem').default[]>} */
  const items = ref([]);
  const worker = ref(new StorageWorker())
  const fetching = ref(false);
  const loading = ref(false);
  const isNewDocument = ref(false);

  const { dictionary } = useTranslator();
  const router = useRouter();
  const storage = useStorage();
  const {fetchItems, sendItems} = useService()
  const {openEditModal} = useEditModal(items)

  onIonViewWillEnter(async () => {  
    const workerJson = await storage.service.get(storage.keys.worker)
    worker.value = JSON.parse(workerJson)
    isNewDocument.value = worker.value.isNewDocument

    // in case if back from ScanItemPage
    const itemsJson = await storage.service.get(storage.keys.items);

    if(!itemsJson){
      if(!isNewDocument.value){
        fetching.value = true;
        fetchItems(worker.value)
          .then(data=> items.value = data)
          .finally(()=>fetching.value = false)
      }
    } else {
      items.value = JSON.parse(itemsJson)
    }
  });

  function canSendItems(){
    return !!items.value.length
  }

  async function clearItem(index) {
    items.value[index].scanned = 0;
    items.value[index].placeCode = '';
  }

  // TODO: response handling
  async function submit() {
    if (!items.value || items.value.length === 0) return;

    if(!isNewDocument.value){
      for (let i = 0; i < items.value.length; i++) {
        const item = items.value[i];
        if (item.scanned !== item.planToScan) {
          await showToast(dictionary.value.pageListNotComplete);
          return;
        }
      }
    }

    loading.value = true;
    sendItems(items.value)
      .then(async ()=>{
        await showToast(dictionary.value.pageListItemsSent);
        router.push(routes.worker.path)
      })
      .finally(loading.value=false)
  }

  async function goBack() {
    await storage.service.remove(storage.keys.items);
    router.push(routes.worker.path)
    items.value = []
  }

  function add(){
    router.push(routes.scanItem.path)
  }

  return {
    dictionary,
    router,
    worker,
    items,
    loading,
    fetching,
    isNewDocument,
    add,
    clearItem,
    openEditModal,
    canSendItems,
    submit,
    goBack
  }
}