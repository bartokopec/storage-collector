import { focusInp } from '@/libs/Focus';
import { useTranslator } from '@/libs/Translator';
import { useStorage } from '@/libs/LocalStorage';
import {
  onIonViewWillEnter
} from '@ionic/vue';
import { ref } from 'vue';
import { useRouter } from 'vue-router';
import { routes } from '@/router';
import StorageWorker from '@/models/StorageWorker';
import {useService} from './useService'
import {useModal} from '@/hooks'
import '@/libs/types'
import { ScanEmptyDocument } from '@/components/molecules';

export function pageController(){
  const { dictionary } = useTranslator();
  const router = useRouter();
  const storage = useStorage();
  const {getDocuments} = useService()
  const {openModal} = useModal(ScanEmptyDocument)

  const userCode = ref('');
  const savedUserCode = ref('');
  /** @type {import('vue').Ref<StorageDocument[],StorageDocument[]>} */
  const docs = ref([]);
  const loading = ref(false);

  onIonViewWillEnter(() => {
    if (!userCode.value) {
      setTimeout(() => {
        focusInp('user-code');
      }, 500);
    } else {
      submit();
    }
  });

  function submit() {
    loading.value = true;
    docs.value = [];
    getDocuments(userCode.value)
      .then((records)=>{
        docs.value = records;
        savedUserCode.value = userCode.value
      })
      .finally(()=>loading.value = false)
  }

  /** @param {string} selectedDocument  */
  async function select(selectedDocument, isNewDocument = false){
    const worker = new StorageWorker(userCode.value, selectedDocument, isNewDocument)
    await storage.service.set(storage.keys.worker, JSON.stringify(worker))
    await storage.service.remove(storage.keys.items);   
    router.push(routes.list.path)
  }

  async function createNewDocument(){
    await openModal({
      submit: (newDocNumber)=>{
        select(newDocNumber, true)
      }
    })
  }

  async function logout() {
    await storage.service.remove(storage.keys.docs);
    await storage.service.remove(storage.keys.worker);
    await storage.service.remove(storage.keys.items);
    docs.value = []
    userCode.value = ''
  }

  return {
    dictionary,
    loading,
    userCode,
    savedUserCode,
    docs,
    logout,
    submit,
    select,
    createNewDocument
  }
}