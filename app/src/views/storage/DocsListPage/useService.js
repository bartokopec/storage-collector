import { useApi } from '@/hooks/useApi';
import { ApiRequest } from '@/models/ApiRequest';
import { showToast } from '@/libs/Toast';
import { env } from '@/libs/envs';
import { useTranslator } from '@/libs/Translator';
import '@/libs/types'

export function useService(){
  const { dictionary } = useTranslator();
  
  /**
   * @param {string} userCode 
   * @returns {Promise<StorageDocument[]>}
   */
  function getDocuments(userCode){
    return new Promise((resolve, reject)=>{
      return useApi(
        new ApiRequest('GET', env.API_ROUTE_DOCS, {
          login: userCode
        }),
        async (resp) => {
          /** @type {StorageDocument[]} */
          const records = resp.getPayload();
  
          if (records.length === 0) {
            await showToast(dictionary.value.pageDoclistNotFound);
            resolve([])
            return;
          }
  
          resolve(records)
        },
        (err) => {
          // eslint-disable-next-line no-console
          console.error(err);
          reject(err)
        }
      );
    })
  }

  return {
    getDocuments
  }
}