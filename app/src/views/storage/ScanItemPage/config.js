import '@/libs/types'
import {env} from '@/libs'

export const inputIdPlace = 'product-place'
export const inputIdCode = 'product-code'

/** @type {MaskingIntelligenceData} */
export const masksData = [
  { id: inputIdCode, mask: env.MASK_ITEM_CODE },
  { id: inputIdPlace, mask: env.MASK_ITEM_PLACE }
]
