import { focusInp } from '@/libs/Focus';
import { useTranslator } from '@/libs/Translator';
import { useStorage } from '@/libs/LocalStorage';
import { onIonViewDidEnter } from '@ionic/vue';
import { ref } from 'vue';
import { useRouter } from 'vue-router';
import StorageItem from '@/models/StorageItem';
import StorageWorker from '@/models/StorageWorker';
import { showToast } from '@/libs/Toast';
import {useMaskingIntel} from '@/hooks/useMaskingIntel'
import {masksData} from './config'
import {env} from '@/libs'

export function pageController(){
  const { dictionary } = useTranslator();
  const router = useRouter();
  const storage = useStorage();
  const {findMatchMask} = useMaskingIntel(masksData)

  const storageCode = ref('');
  const productCode = ref('');
  const loading = ref(false);
  /** @type {import('vue').Ref<StorageItem[], StorageItem[]>} */
  const items = ref([]);
  const worker = ref(new StorageWorker());
  const lastScanned = ref(new StorageItem());
  const totalCount = ref('0');
  const isNewDocument = ref(false);

  onIonViewDidEnter(async () => {
    setTimeout(() => {
      focusInp(storageCode.value ? 'product-code' : 'product-place');
    }, 500);
    worker.value = JSON.parse(await storage.service.get(storage.keys.worker));
    const savedItems = JSON.parse(await storage.service.get(storage.keys.items));
    if(savedItems){
      items.value = savedItems
    }
    if (!worker.value) {
      goBack();
    }

    isNewDocument.value = worker.value.isNewDocument
    readTotalSum();
  });

  function goBack(){
    router.back()
  }

  function readTotalSum() {
    if(!items.value || !items.value.length) return;
    totalCount.value = !isNewDocument.value
      ? `${items.value.reduce((partial, item) => partial + item.scanned, 0)}/${items.value.reduce((partial, item) => partial + item.planToScan, 0)}`
      : `${items.value.reduce((partial, item) => partial + item.scanned, 0)}`
  }

  /** @param {StorageItem} item */
  function setLastScanned(item){
    lastScanned.value = new StorageItem(
      null,
      null,
      item.productCode,
      item.planToScan,
      null,
      item.scanned
    );
  }

  async function finishSaving(){
    await storage.service.set(storage.keys.items, JSON.stringify(items.value));
    await showToast(dictionary.value.pageStorageToastAdded);
    productCode.value = '';
    readTotalSum();
  }

  async function save() {
    if (!worker.value) {
      goBack();
      await showToast(dictionary.value.pageStorageWorkerMissing);
      return;
    }

    const place = storageCode.value.trim();
    const code = productCode.value.trim();

    if (!place.match(env.MASK_ITEM_PLACE) || !code.match(env.MASK_ITEM_CODE)) {
      await showToast(dictionary.value.maskNotMatch);
      return;
    }

    loading.value = true;
    const found = items.value.find((item) => item.productCode === code)
    if (found) {
      const item = found;
      if (item.productCode === code) {          
        if (item.scanned === item.planToScan) {
          await showToast(dictionary.value.pageStorageProductIsComplete);
          loading.value = false;
          return;
        }
        item.scanned += 1;
        item.placeCode = place;
        setLastScanned(item)
      }
      finishSaving()
    } else {
      if(isNewDocument.value){
        const newItem = new StorageItem(
          worker.value,
          place,
          code,
          -1,
          '',
          1
        )
        items.value.push(newItem)
        setLastScanned(newItem)
        finishSaving()
      } else {
        await showToast(dictionary.value.pageStorageProductMissing);
      }
    }

    loading.value = false;

    setTimeout(() => {
      focusInp('product-code');
    }, 500);
  }

  return {
    dictionary,
    worker,
    lastScanned,
    totalCount,
    storageCode,
    productCode,
    loading,
    isNewDocument,
    save,
    goBack,
    focusInp,
    findMatchMask
  }
}