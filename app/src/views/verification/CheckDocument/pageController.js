import { focusInp } from '@/libs/Focus';
import { useTranslator } from '@/libs/Translator';
import { onIonViewDidEnter } from '@ionic/vue';
import { ref } from 'vue';
import { showToast } from '@/libs/Toast';
import { useApi } from '@/hooks/useApi';
import { ApiRequest } from '@/models/ApiRequest';
import { env } from '@/libs/envs';

export function pageController() {
  const { dictionary } = useTranslator();

  const loading = ref(false);
  const docNumber = ref('');
  const docNumberError = ref('');
  const records = ref([]);

  onIonViewDidEnter(() => {
    setTimeout(() => {
      focusInp('docdetails-number');
    }, 1000);
  });

  async function submit() {
    if (!docNumber.value) {
      docNumberError.value = dictionary.value.pageDocFormInputError;
      return;
    }

    loading.value = true;
    const doc = docNumber.value.trim();

    await useApi(
      new ApiRequest('GET', env.API_ROUTE_DOCDETAILS, {
        doc
      }),
      async (resp) => {
        loading.value = false;
        records.value = resp.getPayload();

        if (records.value.length === 0) {
          await showToast(dictionary.value.pageDocFormEmpty);
        }        
      },
      (err) => {
        // eslint-disable-next-line no-console
        console.error(err);
        loading.value = false;
      }
    );
    loading.value = false;
  }

  /** @param {string} value  */
  function updateDocNumber(value){
    docNumber.value = value;
    records.value = []
    docNumberError.value = ''
  }

  return {
    dictionary,
    docNumber,
    docNumberError,
    loading,
    records,
    submit,
    updateDocNumber
  }
}