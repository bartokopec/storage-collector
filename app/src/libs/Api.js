import axios from 'axios';
import { ApiResponse } from '../models/ApiResponse';
import { Statuses } from '../models/ApiStatuses';
import { useStorage } from '../libs/LocalStorage';

// TODO: https://github.com/axios/axios#cancellation
// TODO: tests

/**
 * @typedef {import('./../models/ApiRequest').default} ApiRequest
 * @param {ApiRequest} request
 * @returns {Promise<ApiResponse>}
 */
export async function http(request) {
  const { method, endpoint, data } = request;
  const storage = useStorage();

  const host = await storage.service.get(storage.keys.api);
  const creds = await storage.service.get(storage.keys.apiKey);

  if (!host || !creds) {
    return new ApiResponse(Statuses.credsNotFound, undefined);
  }

  const withQuery = method === 'GET' && !!data;
  let queredEndpoint = endpoint + '?';
  if (withQuery) {
    for (const key in data) {
      const value = data[key];
      queredEndpoint += `${key}=${value}&`;
    }
  }

  try {
    const response = await axios.request({
      method,
      url: host + (withQuery ? queredEndpoint : endpoint),
      headers: {
        Authorization: creds
      },
      data
    });
    return new ApiResponse(Statuses.success, response.data);
  } catch (error) {
    // TODO: more errors handling
    if (error.code === 'ERR_NETWORK') {
      return new ApiResponse(Statuses.unreachable, undefined);
    }
    switch (error.response.status) {
      case 400:
        return new ApiResponse(Statuses.badRequest, error.response.data);
      case 401:
        return new ApiResponse(Statuses.unauthorization, error.response.data);
      default:
        return new ApiResponse(Statuses.unknown, error);
    }
  }
}
