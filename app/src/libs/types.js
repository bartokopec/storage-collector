/**
 * @typedef StorageDocument
 * @property {string} date
 * @property {string} document
 * @property {string} zone
 * @property {number} quantity
 * @property {string} description
 */

/**
 * @typedef DocInfoResponse
 * @property {{barcode: string, quantity: number, description: string}[]} document
 */

/**
 * @typedef {{id: string, mask: string}[]} MaskingIntelligenceData
 */