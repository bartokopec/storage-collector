// import { BarcodeScanner, LensFacing } from '@capacitor-mlkit/barcode-scanning';
import { ref } from 'vue';

const canCameraScan = ref(false);

export function useCameraScan() {
  const setCanCameraScan = (val) => (canCameraScan.value = val);
  return {
    canCameraScan,
    setCanCameraScan
  };
}

/**
 * @param {(value: string)=>void} onScan
 * @returns {Promise<string>}
 */
export async function scan() {
  // if (!(await BarcodeScanner.isGoogleBarcodeScannerModuleAvailable())) {
  //   await BarcodeScanner.installGoogleBarcodeScannerModule();
  // }
  // // TODO: CHANGE SETTING CSS CLASS TO OTHER ELEMENT, NOT BODY.
  // // IT COULD FIX MISSING CONTROL LAYOUT WHEN SCANNING
  // document.querySelector('body')?.classList.add('barcode-scanner-active');
  // const listener = await BarcodeScanner.addListener('barcodeScanned', async (result) => {
  //   await listener.remove();
  //   document.querySelector('body')?.classList.remove('barcode-scanner-active');
  //   await BarcodeScanner.stopScan();
  //   onScan(result.barcode.rawValue);
  // });

  // await BarcodeScanner.startScan({
  //   lensFacing: LensFacing.Back
  // });
}
