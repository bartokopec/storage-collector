export function focus(id) {
  document.getElementById(id).focus();
}

export function focusInp(id) {
  const container = document.getElementById(id);
  if (container) {
    container.getElementsByTagName('input')[0].focus();
  }
}
