import { Device } from '@capacitor/device';
import { ref } from 'vue';
import DictionaryModel from '../localization/DictionaryModel';
import dictPL from '../localization/pl.json';
import dictEN from '../localization/en.json';
import { useStorage } from './LocalStorage';

const langCodes = {
  PL: dictPL.__info,
  EN: dictEN.__info
};
const dictionary = ref(new DictionaryModel());
const langCode = ref('');

export class TranslatorService {
  /** @param {import('./LocalStorage').StorageService} storagee */
  constructor(storage) {
    this._storage = storage;
    this.setSavedLanguage();
  }

  async setSavedLanguage() {
    const deviceLang = await Device.getLanguageCode();
    const savedCode =
      (await this._storage.service.get(this._storage.keys.lang)) || deviceLang.value;

    await this.setLanguage(savedCode);
  }

  /** @param {string} code */
  async setLanguage(code) {
    langCode.value = code;
    await this._storage.service.set(this._storage.keys.lang, code);
    switch (code) {
      case langCodes.PL.code:
        dictionary.value = dictPL;
        break;
      default:
        dictionary.value = dictEN;
        break;
    }
  }
}

// TODO: make it better
export function useTranslator() {
  const storage = useStorage();
  const translator = new TranslatorService(storage);

  return {
    dictionary: dictionary,
    langCode: langCode,
    translator,
    langCodes: langCodes
  };
}
