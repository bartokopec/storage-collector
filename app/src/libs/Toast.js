import { toastController } from '@ionic/vue';

/**
 * @param {string} message
 * @returns {Promise<void>}
 */
export async function showToast(message) {
  const toast = await toastController.create({
    message,
    duration: 3000,
    position: 'middle'
  });
  await toast.present();
}
