import { ref } from 'vue';
import { useStorage } from '../libs/LocalStorage';

const storageKey = 'use-dark';
const paletteToggle = ref(false);

// Use matchMedia to check the user preference
const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');

// Add or remove the "ion-palette-dark" class on the html element
const toggleDarkPalette = (shouldAdd) => {
  useStorage().service.set(storageKey, shouldAdd);
  paletteToggle.value = shouldAdd;
  document.documentElement.classList.toggle('ion-palette-dark', shouldAdd);
};

// Check/uncheck the toggle and update the palette based on isDark
const initializeDarkPalette = (isDark) => {
  paletteToggle.value = isDark;
  toggleDarkPalette(isDark);
};

// Initialize the dark palette based on the initial
// value of the prefers-color-scheme media query
useStorage()
  .service.get(storageKey)
  .then((val) => {
    initializeDarkPalette(val);
  });

// Listen for changes to the prefers-color-scheme media query
prefersDark.addEventListener('change', (mediaQuery) => initializeDarkPalette(mediaQuery.matches));

export function useDarkTheme() {
  return {
    paletteToggle,
    toggleDarkPalette
  };
}
