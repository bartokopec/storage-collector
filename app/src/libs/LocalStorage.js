import { Storage, Drivers } from '@ionic/storage';
// import { SecureStorage } from '@aparajita/capacitor-secure-storage';

const storageKeys = {
  docdetails: 'docdetails',
  item: 'item',
  fontSize: 'fontSize',
  lang: 'lang',
  worker: 'worker',
  items: 'items',
  api: 'api',
  apiKey: 'apiKey',
  cameraScan: 'cameraScan',
  docs: 'docs',
  newDoc: 'newDoc'
};

export class StorageService {
  // TODO: change this code, to creating new storage in each functional
  // methods (not using singleton): set, get, remove
  async init() {
    if (StorageService._storage) {
      return;
    }
    const storage = await new Storage({
      name: 'storage-collector',
      driverOrder: [Drivers.IndexedDB, Drivers.LocalStorage]
    }).create();
    this._storage = storage;
  }

  /**
   * @param {string} key
   * @param {any} value
   */
  async set(key, value) {
    await this.init();
    await this._storage?.set(key, value);
  }

  /**
   * @param {string} key
   * @returns {Promise<any>}
   */
  async get(key) {
    await this.init();
    return this._storage?.get(key);
  }

  /** @param {string} key */
  async remove(key) {
    await this.init();
    await this._storage?.remove(key);
  }

  /**
   * @param {string} key
   * @param {string} value
   */
  // async secureSet(key, value) {
  //   await SecureStorage.setItem(key, value);
  // }

  /**
   * @param {string} key
   * @returns {Promise<string>}
   */
  // async secureGet(key) {
  //   await SecureStorage.getItem(key);
  // }
}

export function useStorage() {
  const service = new StorageService();

  return { keys: storageKeys, service };
}
