import { createRouter, createWebHistory } from '@ionic/vue-router';
import Pages from './Pages.vue';

export const routes = {
  worker: {
    path: '/storage',
    name: 'DocsList',
    component: () => import('./views/storage/DocsListPage/DocsListPage.vue')
  },
  scanItem: {
    path: '/storage/scan',
    name: 'Storage',
    component: () => import('./views/storage/ScanItemPage/ScanItemPage.vue')
  },
  list: {
    path: '/storage/list',
    name: 'List',
    component: () => import('./views/storage/DocItemsPage/DocItemsPage.vue')
  },
  settings: {
    path: '/settings',
    name: 'Settings',
    component: () => import('./views/SettingsPage.vue')
  },
  verification: {
    path: '/verification',
    name: 'Verification',
    component: () => import('./views/verification/CheckDocument/CheckDocument.vue')
  }
};

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      redirect: '/storage'
    },
    {
      path: '/',
      component: Pages,
      children: [
        {
          path: '',
          redirect: '/storage'
        },
        routes.worker,
        routes.scanItem,
        routes.list,
        routes.settings,
        routes.verification
      ]
    }
  ]
});

/**  */
export default router;
