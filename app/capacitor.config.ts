import { CapacitorConfig } from '@capacitor/cli';
import appInfo from './package.json' with {type:'json'};
import dotenv from 'dotenv'
dotenv.config()

const config: CapacitorConfig = {
  appId: appInfo.appId,
  appName: appInfo.appName,
  webDir: 'dist',
  server: {
    androidScheme: 'https'
  },
  // android:{
  //   buildOptions:{
  //     keystorePath: process.env.VITE_KEYSTORE_PATH,
  //     keystorePassword: process.env.VITE_KEYSTORE_PASS,
  //     keystoreAlias: process.env.VITE_KEYSTORE_ALIAS,
  //     keystoreAliasPassword: process.env.VITE_KEYSTORE_ALIAS_PASS,
  //     releaseType: 'APK',
  //     // signingType: 'apksigner',
  //     //androidReleaseType: 'APK'
  //   }
  // },
  plugins: {
    StatusBar: {
      overlaysWebView: false,
    }
  }
};

export default config;
