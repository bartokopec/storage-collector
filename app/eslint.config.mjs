import pluginVue from 'eslint-plugin-vue'
import eslintJS from '@eslint/js'

export default [
  {
    ignores: [
      'node_modules/*',
      '**/dist',
      '**/android',
      '**/.eslintrc.cjs',
      '**/pnpm-lock.yaml',
      '**/package-lock.json',
      'package.mjs'
    ]
  },

  ...pluginVue.configs['flat/recommended'],
  
  {
    files: ['src/**/*.js', 'src/**/*.vue', 'scripts/**/*.js'],
    languageOptions: {
      ecmaVersion: 2022,
      sourceType: 'module'
    },
    settings: {
      'prettier-vue': {
        SFCBlocks: {
          template: true,
          script: true,
          style: true
        },
        usePrettierrc: true
      }
    },
    rules: {
      ...eslintJS.configs.recommended.rules,
      indent: [
        'error',
        2,
        {
          SwitchCase: 1
        }
      ],
      'no-console': 'error',
      'no-debugger': 'error',
      'vue/no-deprecated-slot-attribute': 'off',
      camelcase: 'off',
      'vue/multi-word-component-names': 'off',
      'vue/require-default-prop': 'off'
    }
  }
];
