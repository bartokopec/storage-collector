// it's very simple api, just for tests.

const auth = '123'
const userCode = '0909'
const userCodeEmpty = '0505'

import dotenv from 'dotenv'
import {createServer} from 'node:http'

dotenv.config()

export const routes = {
  API_ROUTE_STORAGE: process.env.VITE_API_ROUTE_STORAGE,
  API_ROUTE_CHECK: process.env.VITE_API_ROUTE_CHECK,
  API_ROUTE_DOCINFO: process.env.VITE_API_ROUTE_DOCINFO,
  API_ROUTE_DOCS: process.env.VITE_API_ROUTE_DOCS,
  API_ROUTE_DOCDETAILS: process.env.VITE_API_ROUTE_DOCDETAILS
};

const headers = {
  'Access-Control-Allow-Headers': 'authorization,content-type',
  'Access-Control-Allow-Methods': 'OPTIONS, POST, GET',
  'Access-Control-Max-Age': 2592000,
  'Content-Security-Policy': "default-src 'self';base-uri 'self';font-src 'self' https: data:;form-action 'self';frame-ancestors 'self';img-src 'self' data:;object-src 'none';script-src 'self';script-src-attr 'none';style-src 'self' https: 'unsafe-inline';upgrade-insecure-request",
  'Cross-Origin-Opener-Policy': 'same-origin',
  'Cross-Origin-Resource-Policy': 'same-origin',
  'Origin-Agent-Cluster': '?1',
  'Referrer-Policy': 'no-referrer',
  'Strict-Transport-Security': 'max-age=15552000; includeSubDomains',
  'X-Frame-Options': 'SAMEORIGIN',
  'X-Permitted-Cross-Domain-Policies': 'none',
  'X-XSS-Protection': 0,
  'X-DNS-Prefetch-Control': 'off',
  'X-Content-Type-Options': 'nosniff',
  'Access-Control-Expose-Headers': '*'
};

const api = createServer()

api.on('request',(request)=>{
    // READ BODY
  request.bodyChunks = []
  request.on('data', chunk => {
    request.bodyChunks.push(chunk);
  })
  request.on('end', ()=>{
    const body = Buffer.concat(request.bodyChunks).toString();
    request.body = body
  })

  // READ QUERY PARAMS
  request.on('end', ()=>{
    request.params = {}
    if(request.url.split('?')?.[1]){
      request.url.split('?')?.[1].split('&').forEach(keyValue=>{
        const record = keyValue.split('=')
        request.params[record[0]] = record[1]
      })
    }
  })
})

api.on('request',(req, res)=>{
  req.on('end', ()=>{   
    const responseHeaders = {...headers, 'Access-Control-Allow-Origin': req.headers.origin ?? ''}         
    // NOT AUTHORIZED

    if(req.method !== 'GET' && req.method !== 'POST'){
      res.writeHead(200, responseHeaders)
      res.end();
      return;
    }
    
    if(req.url === routes.API_ROUTE_CHECK){
      console.log(routes.API_ROUTE_CHECK);
      res.writeHead(200, responseHeaders)
      res.end('OK')
      return;
    }
    
    // AUTHORIZATION GATE
    if(req.headers['authorization'] !== auth){
      console.log('401');
      res.writeHead(401, responseHeaders)
      res.end()
      return
    }

    // GET ?login=<string>
    if(req.url.startsWith(routes.API_ROUTE_DOCS)){
      console.log(routes.API_ROUTE_DOCS, req.params);
      res.writeHead(200, responseHeaders)
      if(req.params['login'] === userCode){
        res.end(JSON.stringify([
          {
            date: '2025-01-01',
            zone: 'Z1',
            document: 'QWE67808765434567',
            quantity: 3,
            description: 'very small document of items',
          },
          {
            date: '2025-01-02',
            zone: 'Z2',
            document: 'QWE123',
            quantity: 4,
            description: 'another very small document of items',
          },
          {
            date: '2025-01-02',
            zone: 'Z2MNMNMNMMNMNMkkkkkkkkkkkkkkk',
            document: 'QWE123',
            quantity: 4,
            description: 'another very small document of items',
          },
        ]))
      } else if(req.params['login'] === userCodeEmpty) {
        res.end(JSON.stringify([]))
      } else {
        res.end()
      }
      return;
    } 

    // GET ?id=<string>&doc=<string>. Should return correct, 
    // as this endpoint is calling after document selection from list
    if(req.url.startsWith(routes.API_ROUTE_DOCINFO)){
      console.log(routes.API_ROUTE_DOCINFO);
      res.writeHead(200, responseHeaders)
      const id = req.params['id']
      const doc = req.params['doc']
      console.log(id, doc);
      
      res.end(JSON.stringify({
        document: [
          {barcode: '1234567890', quantity: 4, description: 'Simple just item one'},
          {barcode: '1234567890123', quantity: 2, description: 'Simple just item two'},
          {barcode: '1234567890111', quantity: 1, description: 'Simple just item three'},
        ]
      }))
      return;
    } 

    // POST {items:<StorageItem DTO>[]}
    if(req.url.startsWith(routes.API_ROUTE_STORAGE)){
      console.log(routes.API_ROUTE_STORAGE);
      res.writeHead(200, responseHeaders)
      console.log(req.body);
      res.end()
      return;
    } 

    // GET ?doc=<string>
    if(req.url.startsWith(routes.API_ROUTE_DOCDETAILS)){
      console.log(routes.API_ROUTE_DOCDETAILS);
      res.writeHead(200, responseHeaders)
      res.end(JSON.stringify([
        'data: 2025-01-01',
        'numer zam: 100000',
        'metoda płatności: TEST',
        'transport: WŁASNY',
        'nazwa sklepu: S001',
        '',
        'NR0001 - AB1 KOLOR - PRZEDMIOT',
        'NR0002 - AB2 KOLOR - PRZEDMIOT',
        'NR0003 - AB3 KOLOR - PRZEDMIOT',
        'NR0004 - AB4 KOLOR - PRZEDMIOT',      
      ]))
      return;
    } 
  })
})

api.listen(8080, ()=> {
  console.info('Mock API listening on port 8080')
})