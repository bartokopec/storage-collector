# Storage collector mobile application

## Frameworks in use

- Ionic
- Vue
- Android

# Requirements

- NodeJS v20 or higher
- Java v17 (https://github.com/ionic-team/capacitor/issues/7166)

## Configuration with `.env` file

https://vitejs.dev/guide/env-and-mode

Variables must start with `VITE_` due to vitejs and because it compiles client side.

```
VITE_API_ROUTE_STORAGE="/api/collect"
VITE_API_ROUTE_DOCINFO="/api/docinfo"
VITE_API_ROUTE_DOCDETAILS="/api/docdetails"
VITE_API_ROUTE_DOCS="/api/docs"
VITE_API_ROUTE_CHECK="/api/check"

# for Android APK signing process
VITE_KEYSTORE_PATH="/absolute/path"
VITE_KEYSTORE_PASS="password"
VITE_KEYSTORE_ALIAS="alias"
VITE_KEYSTORE_ALIAS_PASS="alias pass"
```

## COMMANDS

are in `package.json` in `script` section:

Running on localhost: `pnpm dev`.

Preparation before pushing new commit: `pnpm pre:commit` - it runs eslint and Vite build (to check build process).

When dir `android` is missing: `npx cap add android`.

Preparation for compilation: `pnpm pre:compile` - it runs Vite build, sync Vite builded app with Android app, generate for Android any assets like icons or splash screen.

Compiling the APK: `pnpm compile` - it runs Trapeze program (overwriting Androidmanifest meta infos), (**BEFORE THIS**, THE `ANDROID_HOME` SYSTEM ENV VARIABLE MUST BE SET, FOR EXAMPLE `export ANDROID_HOME=/home/user/Android/Sdk/`) builds Android APK app with CapacitorJS, signs builded APK with `apksigner.js`.

---
CapacitorJS CLI docs: https://capacitorjs.com/docs/cli

---

## Branches workflow

- Each issue or many issues have to be done in separated branch with name came from ID(s) of issue(s), for example `bug/#99`,
- next, there will be merge to main on repo page,
- after merge, there will be publishing new release.

## Android developing

[https://capacitorjs.com/docs/getting-started/with-ionic#ionic-cli-capacitor-commands](https://capacitorjs.com/docs/getting-started/with-ionic#ionic-cli-capacitor-commands)

Tool for generating icons: 

- [https://github.com/ionic-team/capacitor-assets](https://github.com/ionic-team/capacitor-assets)
- [https://capacitorjs.com/docs/guides/splash-screens-and-icons](https://capacitorjs.com/docs/guides/splash-screens-and-icons)

## Dependencies

### Localizations / i18n

[https://ionicframework.com/docs/native/device](https://ionicframework.com/docs/native/device)

Use of native interface, which returns device language tag:

[https://en.wikipedia.org/wiki/IETF_language_tag#List_of_common_primary_language_subtags](https://en.wikipedia.org/wiki/IETF_language_tag#List_of_common_primary_language_subtags)

### Barcodes scanning

(experimental)

https://github.com/capawesome-team/capacitor-mlkit/tree/main/packages/barcode-scanning

## API

[https://codeberg.org/bartokopec/storage-collector-api](https://codeberg.org/bartokopec/storage-collector-api)

Should have such endpoints:

### POST Send collected data

`/api/collect`

Body:

```
{
    "items": [
        {
            "userCode": "string",
            "docNumber": "string",
            "placeCode": "string",
            "productCode": "string",
            "count": 0
        }
    ]
}
```

**Success return:**

Status: 201

Body:

```
{
    "ids": [ "" ]
}
```

Array of "ids" contains strings.

**Error return:**

```
{
    "error": "message"
}
```